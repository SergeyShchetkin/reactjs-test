var path = require('path');
var webpack = require('webpack');

var webpackConfig = {
    entry: './app/components/js/main.js',
    output: {
        path: './public/build/',
        filename: 'bundle.js'
    },
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|vendor)/,
                loader: 'babel',
                query: {
                    presets: ['react', 'es2015']
                }
            },
            {
                test: /\.css$/,
                loader: "css-loader"
            },
            {
                test: /\.scss$/,
                loader: "css-loader!sass-loader"
            },
            {
                test: /\.less$/,
                loader: "css-loader!less-loader"
            },
            {
                test: /\.(png|jpg|gif)$/,
                loader: "file-loader?name=img/img-[hash:6].[ext]"
            }
        ]
    },
    debug: true
};

module.exports = webpackConfig;