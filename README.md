# *** ReactJS First Project ***

### Test application study

---

##### Libraries:

* [Yenoman](http://yeoman.io/)

* [Webpack](https://webpack.github.io/)

* [Babel](https://babeljs.io/)

* [ReactJS](https://facebook.github.io/react/)

##### Installation and configuration:

*Before using you must install all the necessary components of the system:*

```
> npm install -g bower webpack yo 
> npm install
> bower install
```


#### Start the server

```
> webpack
```
---
