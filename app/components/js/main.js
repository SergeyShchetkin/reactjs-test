var React = require('react');

var ReactDOM = require('react-dom');

var Main = React.createClass({
    render: function() {
        return (
            <div class='alert alert-info'>
                <p>Hello World!!!</p>
                It is {this.props.date.toTimeString()}
            </div>
        )
    }
});

setInterval(function() {
    ReactDOM.render(
        <Main date={new Date()} />,
        document.getElementById('app')
    );
}, 500);